# Torrent Client

Torrent Client simple UI. Additional 'Search Engines' can be found here:

* [Search Engines](https://axet.gitlab.io/android-torrent-client/)

Real pirates steal author's rights, don't be like them - share!

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Torrent Client' to your language  please read following:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

Revive your **old android** device (android 2.3.3 and up) and start persistent torrent share!

![shot](/docs/old.png) ![+](/docs/plus.png) ![shot](/docs/shot.png) ![=](/docs/equals.png) ![shot](/docs/new.png)
