package com.github.axet.torrentclient.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.torrentclient.activities.BootActivity;
import com.github.axet.torrentclient.app.TorrentApplication;

public class OnExternalReceiver extends com.github.axet.androidlibrary.services.OnExternalReceiver {
    @Override
    public void onBootReceived(Context context) {
        OptimizationPreferenceCompat.setPrefTime(context, TorrentApplication.PREFERENCE_BOOT, System.currentTimeMillis());

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean(TorrentApplication.PREFERENCE_START, false)) {
            BootActivity.createApplication(context);
            return;
        }

        if (shared.getBoolean(TorrentApplication.PREFERENCE_RUN, false)) {
            Log.d(TAG, "Restart Application");
            BootActivity.createApplication(context);
            return;
        }
    }
}
